from setuptools import setup, find_packages

setup(
    name='typsytex',
    version='1.0.1',
    install_requires=[],
    entry_points={
        'console_scripts': [
            'typsytex = typsytex:main'
        ],
    },
)
